from flask import Blueprint, request
from database import Database
import json


api = Blueprint('api', __name__)


@api.route('/entries')
def get_all_entries():
    entries = Database.get_records()
    for entry in entries:
        entry['_id'] = str(entry['_id'])
    return json.dumps(entries)


@api.route('/post', methods=['POST'])
def post_an_entry():
    doc = {
        'title': request.form['title'],
        'post': request.form['post']
    }
    Database.insert_record(doc)
    return 'Record successfully added.'


@api.route('/delete', methods=['DELETE'])
def delete_all_entries():
    Database.delete_records()
    return 'Records successfully deleted.'
