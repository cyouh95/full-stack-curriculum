import pymongo
import os


class Database:

    DB = None

    @staticmethod
    def initialize():
        client = pymongo.MongoClient(os.environ['MONGOLAB_URI'])
        Database.DB = client.mydb

    @staticmethod
    def insert_record(doc):
        Database.DB.entries.insert_one(doc)

    @staticmethod
    def get_records():
        records = [doc for doc in Database.DB.entries.find({})]
        return records

    @staticmethod
    def delete_records():
        Database.DB.entries.drop()
